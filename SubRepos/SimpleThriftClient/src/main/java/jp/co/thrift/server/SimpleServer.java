package jp.co.thrift.server;

import jp.co.sample.ResultStruct;
import jp.co.sample.SampleStruct;
import jp.co.sample.ServiceTest;
import org.apache.thrift.TException;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;

public class SimpleServer {
	public static void main(String[] args) throws Exception {
		System.out.println("Thrift Server");

		TServerTransport serverTransport = new TServerSocket(9090);
		final TServer server = new TThreadPoolServer(new TThreadPoolServer.Args(serverTransport).processor(
			new ServiceTest.Processor<ServiceTest.Iface>(new ServiceTest.Iface() {
				public ResultStruct execute(SampleStruct parameter) throws TException {
					System.out.println("Request: " + parameter.getSampleString());
					System.out.println("Request: " + parameter.getSampleInteger());

					ResultStruct resultTest = new ResultStruct();
					resultTest.result = true;

					// try{ Thread.sleep(10000); } catch(Exception e) { e.printStackTrace(); }

					return resultTest;
				}
			})
		));

		new Thread(new Runnable() {
			public void run() {
				System.out.println("Starting the server...");
				server.serve();
				System.out.println("Stopped the server...");
			}
		}).start();
	}
}
