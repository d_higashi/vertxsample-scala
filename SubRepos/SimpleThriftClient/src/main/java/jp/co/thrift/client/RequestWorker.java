package jp.co.thrift.client;

import jp.co.sample.ResultStruct;
import jp.co.sample.SampleStruct;
import jp.co.sample.ServiceTest;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

public class RequestWorker implements Runnable {
	public void run() {
		try {
			System.out.println("Rrquest");
			TTransport transport = new TSocket("localhost", 9090);
			transport.open();

			TProtocol protocol = new TBinaryProtocol(transport);
			ServiceTest.Client client = new ServiceTest.Client(protocol);


			SampleStruct request = new SampleStruct();
			request.setSampleString("Sample Dayo~~");
			request.setSampleInteger(1234560);

			ResultStruct result = client.execute(request);
			System.out.println("Result: " + result.result);

			transport.close();

		} catch(Exception e){
			e.printStackTrace();
		}
	}
}
