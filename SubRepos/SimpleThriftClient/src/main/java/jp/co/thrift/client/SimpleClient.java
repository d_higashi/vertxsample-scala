package jp.co.thrift.client;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class SimpleClient {
	public static void main(String[] args) throws Exception {
		int thread_size = 1;
		Executor executor = Executors.newFixedThreadPool(thread_size);
		for (int cnt = 0; cnt < thread_size; cnt++) {
			executor.execute(new RequestWorker());
		}
	}
}
