package jp.co.vertx;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.impl.LoggerFactory;
import io.vertx.core.AbstractVerticle;

public class ServerSample extends AbstractVerticle {
	public void start() {
		final Logger logger = LoggerFactory.getLogger(ServerSample.class);

		vertx.deployVerticle("jp.co.vertx.NetServer");

		vertx.deployVerticle(
			"jp.co.vertx.AssignmentVerticle",
			new DeploymentOptions().setWorker(true).setInstances(1));

		vertx.deployVerticle(
			"jp.co.vertx.ExecuteWorkerVerticle",
			new DeploymentOptions().setWorker(true).setInstances(4));

		logger.info("Deploy Done");
	}
}
