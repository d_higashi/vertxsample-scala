package jp.co.vertx;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.net.NetServerOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;

public class NetServer extends AbstractVerticle {

	public void start() {
		final EventBus eventBus = vertx.eventBus();

		vertx.createNetServer(
			new NetServerOptions().setPort(9090)
		).connectHandler((netSocket) -> {
			netSocket.handler((buffer) -> {
				System.out.println("Send EventBus Message");
				eventBus.send("thrift", buffer, (handler) -> {
					netSocket.write((Buffer)handler.result().body());
				});
			});
		}).listen();
	}
}
