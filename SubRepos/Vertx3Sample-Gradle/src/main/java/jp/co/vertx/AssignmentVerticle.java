package jp.co.vertx;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.EventBus;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TMessage;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TIOStreamTransport;
import org.apache.thrift.transport.TTransport;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

public class AssignmentVerticle extends AbstractVerticle {
	public void start() {
		final EventBus eventBus = vertx.eventBus();

		eventBus.<Buffer>consumer("thrift", (buffer) -> {
			try {
				ByteArrayInputStream is = new ByteArrayInputStream(buffer.body().getBytes());
				ByteArrayOutputStream os = new ByteArrayOutputStream();

				TTransport transport = new TIOStreamTransport(is, os);
				TProtocol protocol = new TBinaryProtocol(transport, true, true);

				// FIXME: Dirtyな感じ
				is.mark(0);
				TMessage msg = protocol.readMessageBegin();
				String methodName = msg.name;
				is.reset();

				System.out.println("Method Name: " + methodName);

				eventBus.send(methodName, buffer.body(), (handler) -> {
					buffer.reply(handler.result().body());
				});
			} catch (TException te) {
				te.printStackTrace();
			}
		});
	}
}
