package jp.co.vertx;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.EventBus;
import jp.co.sample.ServiceTest;
import jp.co.sample.ServiceTestEntity;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TIOStreamTransport;
import org.apache.thrift.transport.TTransport;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

public class ExecuteWorkerVerticle extends AbstractVerticle {
	public void start() {
		final EventBus eventBus = vertx.eventBus();
		eventBus.<Buffer>consumer("execute", (handler) -> {
			try {
				ServiceTest.Processor<ServiceTestEntity> processor = new ServiceTest.Processor<ServiceTestEntity>(new ServiceTestEntity());

				ByteArrayInputStream is = new ByteArrayInputStream(handler.body().getBytes());
				ByteArrayOutputStream os = new ByteArrayOutputStream();

				TTransport transport = new TIOStreamTransport(is, os);
				TProtocol protocol = new TBinaryProtocol(transport, true, true);

				processor.process(protocol, protocol);
				transport.close();

				handler.reply(Buffer.buffer(os.toByteArray()));
			} catch (TException tx) {
				tx.printStackTrace();
			}
		});
	}
	}
