package jp.co.vertx;

import org.vertx.java.platform.Verticle;

public class ServerSample extends Verticle {
	public void start() {
		container.deployVerticle("jp.co.vertx.NetServer");
		container.deployWorkerVerticle("jp.co.vertx.WorkerVerticle", 12);

		System.out.println("Deploy Done");
	}
}
