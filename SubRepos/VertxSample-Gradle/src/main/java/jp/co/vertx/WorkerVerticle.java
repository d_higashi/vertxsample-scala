package jp.co.vertx;

import io.vertx.rxcore.java.eventbus.RxEventBus;
import io.vertx.rxcore.java.eventbus.RxMessage;
import jp.co.sample.ServiceTest;
import jp.co.sample.ServiceTestEntity;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TIOStreamTransport;
import org.apache.thrift.transport.TTransport;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.platform.Verticle;
import rx.Observable;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

public class WorkerVerticle extends Verticle {
	public void start() {
		final RxEventBus rxEventBus = new RxEventBus(vertx.eventBus());
		Observable<RxMessage<Buffer>> obs = rxEventBus.<Buffer> registerHandler("thrift");

		obs.subscribe((message) -> {
			try {

				Thread.sleep(10000);
				ServiceTest.Processor<ServiceTestEntity> processor = new ServiceTest.Processor<ServiceTestEntity>(new ServiceTestEntity());

				ByteArrayInputStream is = new ByteArrayInputStream(message.body().getBytes());
				ByteArrayOutputStream os = new ByteArrayOutputStream();

				TTransport transport = new TIOStreamTransport(is, os);
				TProtocol protocol = new TBinaryProtocol(transport, true, true);
				processor.process(protocol, protocol);
				transport.close();
				message.reply(new Buffer(os.toByteArray()));
			} catch (TException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}
}
