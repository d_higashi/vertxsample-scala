package jp.co.sample;

import jp.co.sample.ServiceTest.Iface;
import org.apache.thrift.TException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServiceTestEntity implements Iface {
	Logger logger = LoggerFactory.getLogger(getClass());

	public ResultStruct execute(SampleStruct parameter) throws TException {
		logger.debug("Request SampleString: " + parameter.getSampleString());
		logger.debug("Request SampleInteger: " + parameter.getSampleInteger());

		ResultStruct resultTest = new ResultStruct();
			resultTest.result = true;

			return resultTest;
	}
}