package server.vertx.sample

import jp.co.sample.{ResultStruct, SampleStruct, ServiceTest}
import org.apache.thrift.TException
import org.slf4j.LoggerFactory
import org.vertx.scala.core.buffer.Buffer
import org.vertx.scala.core.eventbus.Message
import org.vertx.scala.platform.Verticle
import server.vertx.sample.worker.ThriftWorkHandler

// MEMO: How to run.
// run-main org.vertx.java.platform.impl.cli.Starter run scala:server.vertx.sample.ServerSample
//



class ServerSample extends Verticle {
  override def start(): Unit = {
    logger.debug("Server Start")
    container.deployWorkerVerticle("server.vertx.sample.worker.ThriftWorkVerticle")

    vertx.createNetServer().connectHandler { netSocket =>
      println(s"Request: ${netSocket.remoteAddress()}")

      netSocket.dataHandler { handler: Buffer =>
        println("Send EventBus")
        val replyHandler = new ThriftWorkHandler(netSocket)
        vertx.eventBus.send(
          "thrift",
          "HOGEHOGE",
          { message: Message[String] =>
          })
      }



//      val processor = new Processor(new ServiceTestEntity)
//      val os = new ByteArrayOutputStream()
//
//      netSocket.dataHandler { handler =>
//        val is = new ByteArrayInputStream(handler.getBytes)
//
//        val transport = new TIOStreamTransport(is, os)
//        val protocol = new TBinaryProtocol(transport, true, true)
//        processor.process(protocol, protocol)
//        transport.close()
//
//        // sleep
//        (0 to 50).foreach { cnt => println(cnt); Thread.sleep(100) }
//
//        netSocket.write(Buffer(os.toByteArray))
//
//        netSocket.close()
//      }
    }.listen(8080)
  }
}

class ServiceTestEntity extends ServiceTest.Iface() {
  private val logger = LoggerFactory.getLogger(this.getClass)

  @throws(classOf[TException])
  def execute(parameter: SampleStruct): ResultStruct = {
    logger.debug(s"Request SampleString: ${parameter.getSampleString}")
    logger.debug(s"Request SampleInteger: ${parameter.getSampleInteger}")

    val resultTest: ResultStruct = new ResultStruct
    resultTest.result = true

    resultTest
  }
}

