package server.vertx.sample.worker

import org.vertx.scala.core.Handler
import org.vertx.scala.core.eventbus._
import org.vertx.scala.core.net.NetSocket
import org.vertx.scala.platform.Verticle

class ThriftWorkVerticle extends Verticle {
  override def start(): Unit = {
    println("[ThriftWorkVerticle] TOP")

    vertx.eventBus.registerHandler(
      "thrift",
      { message: Message[String] =>
        println("Receive")

//        val processor = new Processor(new ServiceTestEntity)
//        val is = new ByteArrayInputStream(message.body().getBytes)
//        val os = new ByteArrayOutputStream()
//        val transport = new TIOStreamTransport(is, os)
//        val protocol = new TBinaryProtocol(transport, true, true)
//        processor.process(protocol, protocol)
//        transport.close()
//
//        message.reply(Buffer(os.toByteArray))
        message.reply("FUGAFUGA")
      })

    println("[ThriftWorkVerticle] BOTTOM")
  }
}

class ThriftWorkHandler extends Handler[Message[String]] {
  private var req: NetSocket = _

  def this(req: NetSocket) {
    this()
    this.req = req
  }

  override def handle(message: Message[String]): Unit = {
    req.write(message.body())
    req.close()
  }
}
