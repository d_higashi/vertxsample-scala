package client.thrift.sample

import java.util.concurrent.Executors

import jp.co.sample.{ResultStruct, SampleStruct, ServiceTest}
import org.apache.thrift.protocol.{TBinaryProtocol, TProtocol}
import org.apache.thrift.transport.{TSocket, TTransport}
import org.slf4j.LoggerFactory

object ClientSample {
  def main(args: Array[String]) {
    val thread_size = 2

    val executor = Executors.newFixedThreadPool(thread_size)
    (0 to thread_size).foreach { _ =>
      executor.execute(new MyWorker)
    }
  }
}

class MyWorker extends Runnable {
  private val logger = LoggerFactory.getLogger("ClientSample")

  private val HOST = "localhost"
  private val PORT = 8080

  override def run(): Unit = {
    val transport: TTransport = new TSocket(HOST, PORT)
    transport.open()

    val protocol: TProtocol = new TBinaryProtocol(transport)
    val client: ServiceTest.Client = new ServiceTest.Client(protocol)

    val request: SampleStruct = new SampleStruct
    request.setSampleString("Sample Dayo~~")
    request.setSampleInteger(1234560)

    logger.debug(s"Request sending")
    val result: ResultStruct = client.execute(request)
    logger.debug(s"Request received")

//    logger.info(s"Result: ${result.result}")

    transport.close()
  }
}
