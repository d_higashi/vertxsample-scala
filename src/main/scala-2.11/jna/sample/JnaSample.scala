package jna.sample

import java.nio.IntBuffer

import com.sun.jna.{NativeLibrary, Native, Library}


object JnaSample {
  def main(args: Array[String]) {
    println("JNA Sample")
    val lib = NativeLibrary.getInstance("/Users/daisuke/Desktop/sample.dylib")
    val funcA = lib.getFunction("concatString")
    val func = lib.getFunction("printString")

    val temp = "ZZZZZZZZZ"

    funcA.invokeVoid(Array("aa", "bb", temp))
    println(s"TEMP: $temp")

    func.invokeVoid(Array(temp))
  }
}

