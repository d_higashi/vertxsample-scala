package server.vertx.sample;

import io.vertx.rxcore.java.eventbus.RxEventBus;
import io.vertx.rxcore.java.eventbus.RxMessage;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.platform.Verticle;
import rx.Observable;

public class JNetServer extends Verticle {

	public void start() {
		final RxEventBus rxEventBus = new RxEventBus(vertx.eventBus());
		vertx.createNetServer().connectHandler((netSocket) -> {
				netSocket.dataHandler((buffer) -> {
					System.out.println("Send EventBus Message");

					Observable<RxMessage<Buffer>> obs = rxEventBus.send("thrift", buffer);
					obs.subscribe((message) -> {
						netSocket.write(message.body());
					});
				});
			}
		).listen(8080);
	}
}
