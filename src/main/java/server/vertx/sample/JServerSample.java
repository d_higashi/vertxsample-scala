package server.vertx.sample;

import org.vertx.java.platform.Verticle;

public class JServerSample extends Verticle {
	public void start() {
		container.deployVerticle("server.vertx.sample.JNetServer");
		container.deployWorkerVerticle("server.vertx.sample.thrift.JWorkerVerticle", 16);

		System.out.println("Deploy Done");
	}
}
