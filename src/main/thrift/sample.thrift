namespace java jp.co.sample

struct SampleStruct {
  1: required string SampleString;
  2: optional i32 SampleInteger;
}

struct ResultStruct {
  1: bool result;
}

service ServiceTest {
  ResultStruct execute(1: SampleStruct parameter)
}
