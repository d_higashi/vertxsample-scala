name := "VertXSample"

version := "1.0"

scalaVersion := "2.11.7"

fork := true

libraryDependencies ++= Seq(
  "io.vertx" % "vertx-platform" % "2.1.5",
  "org.apache.thrift" % "libthrift" % "0.9.2",
  "io.vertx" %% "lang-scala" % "1.1.0-M1",
  "io.vertx" % "mod-rxvertx" % "1.0.0-beta4",
  "net.java.dev.jna" % "jna" % "4.1.0",
  "org.javassist" % "javassist" % "3.20.0-GA",
  "ch.qos.logback" % "logback-classic" % "1.1.3",
  "org.slf4j" % "slf4j-api" % "1.7.12",
  "io.reactivex" % "rxjava" % "1.0.12"
)
